<?php


namespace Drupal\metrobank_maillog\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Drupal\Core\Link;

/**
 * Class Display.
 *
 * @package Drupal\metrobank_maillog\Controller
 */
class MetrobankController extends ControllerBase {

  /**
   * showdata.
   *
   * @return string
   *   Return Table format data.
   */
  public function showdata() {

    $result = \Drupal::database()->select('metrobank_maillog', 'n')
    ->fields('n', array('id', 'header_message_id', 'header_from','header_to',
'header_reply_to','header_all','subject','body','sent_date'))
    ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
    ->limit(10)
    ->orderBy('id', 'DESC')
    ->execute()->fetchAllAssoc('id');

     
    // Create the row element.
    $rows = array();
    foreach ($result as $row => $content) {
      $date = date('m-d-Y - h:i',$content->sent_date);
      $subjects = Link::createFromRoute($content->subject, 'metrobank_maillog.logger.details',['id' => $content->id]);
      $rows[] = array(
        'data' => array($content->id, $date, $subjects, $content->header_from,
        $content->header_to ));
    }

    // Create the header.
    $header = array('#','Date', 'subject', 'From', 'To');
    $output['table'] = array(
      '#type' => 'table',    // Here you can write #type also instead of #theme.
      '#header' => $header,
      '#rows' => $rows
    );
    $output['pager'] = [
      '#type' => 'pager',
    ];
    return $output;
  }
 

  public function detailsdata($id) {

  $result = \Drupal::database()->select('metrobank_maillog', 'n')
    ->fields('n', array('id', 'header_from','header_to',
'header_reply_to','header_all','body'))
->condition('id', $id)
    ->execute()->fetchAll();

$body = $result[0]->body;

$body = stripcslashes($body);
  $body = str_replace('"',"",$body);
  $body = str_replace('&nbsp;',"",$body);
  $body = str_replace('<p>',"",$body);
  $body = str_replace('</p>',"",$body);
  $body = str_replace('<br />',"",$body);

$header_all = unserialize($result[0]->header_all);

    return [
      '#theme' => 'metrobank_maillog_details',
      '#data' => $result[0],
      '#body' => $body,
      '#header_all' => $header_all,
    ];
}

public function title($id) {

    $result_new = \Drupal::database()->select('metrobank_maillog', 'n')
        ->fields('n', array('id', 'subject'))
        ->condition('id', $id)
        ->execute()->fetchAll();
    $result = $result_new[0];


    return $result->subject;
  }
}
