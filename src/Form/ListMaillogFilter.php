<?php

namespace Drupal\metrobank_maillog\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class ListMaillogFilter.
 */
class ListMaillogFilter extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'list_maillog_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $type = NULL)
  {

    $query = \Drupal::request();
    $to_date = $query->get("to_date");
    $from_date = $query->get("from_date");
    $from = $query->get("from");
    $to = $query->get("to");
    $subject = $query->get("subject");
    $form['clear_mail_log'] = [
      '#type' => 'fieldset',
    ];

    $form['clear_mail_log']['clear'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Maillog'),
      '#submit' => ['::clearMailLog'],
      '#attributes' => ['class' => ['button', 'button--primary']],
    ];
    $form['title'] = [
      '#type' => 'markup',
      '#markup' => '<div class="form-maillog-tittle"> Filter Mail Log </div>'
    ];

    $form['from_date'] = [
      '#type' => 'date',
      '#date_format' => 'd/m/Y',
      '#title' => $this->t("From Date"),
      '#default_value' => $from_date ?? '',
      '#size' => 30,
      '#prefix' => '<div class="form--inline clearfix">'
    ];
    $form['to_date'] = [
      '#type' => 'date',
      '#date_format' => 'd/m/Y',
      '#size' => 30,
      '#title' => $this->t("To Date"),
      '#default_value' => $to_date ?? '',
    ];
    $form['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t("From"),
      '#size' => 30,
      '#maxlength' => 128,
      '#default_value' => $from ?? '',
    ];
    $form['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t("To"),
      '#size' => 30,
      '#maxlength' => 128,
      '#default_value' => $to ?? '',
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Subject"),
      '#size' => 30,
      '#maxlength' => 128,
      '#default_value' => $subject ?? '',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t("Apply"),
      '#prefix' => '<div class="form-actions">'
    ];
    if ($to_date || $from_date || $from || $to || $subject) {
      $form['actions']['reset'] = array(
        '#type' => 'submit',
        '#value' => t('Reset'),
        '#submit' => array('::resetForm'),
        '#suffix' => '</div></div>'
      );
  }
    $database = \Drupal::database()->select("metrobank_maillog", "ml");

    $database->fields("ml");

    if ($from_date ) {
      $fromDate_timestamp  = strtotime($from_date);
      $database->condition("sent_date", $fromDate_timestamp, ">=");
    }
    if ($to_date ) {
      $toDate_timestamp  = strtotime($to_date);
      $database->condition("sent_date", $toDate_timestamp, "<=");
    }
    if ($from) {
      $database->condition("header_from", "%" . $database->escapeLike($from) . "%", 'LIKE');
    }
    if ($to) {
      $database->condition("header_to", "%" . $database->escapeLike($to) . "%", 'LIKE');
    }
    if ($subject) {
      $database->condition("subject", "%" . $database->escapeLike($subject) . "%", 'LIKE');
    }
    $database->orderBy('id', 'DESC');
    $pager = $database->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute()->fetchAll(\PDO::FETCH_ASSOC);
    // Create the row element.

    $header = [
      '#' => $this->t('#'),
        'Date' => $this->t('Date'),
        'subject' => $this->t('subject'),
        'From' => $this->t('From'),
        'To' => $this->t('To'),
      ];
    $rows = array();
    if ($result) {
      foreach ($result as $content) {
        $date = date('m-d-Y - h:i', $content['sent_date']);
        $subjects = Link::createFromRoute($content['subject'], 'metrobank_maillog.logger.details', ['id' => $content['id']]);
        $rows[$content['id']] = [
          '#' => $content['id'],
          'Date' => $date,
          'subject' => $subjects,
          'From' => $content['header_from'],
          'To' => $content['header_to']
        ];
      }
    }

      $form['table'] = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows ??  '',
        '#multiple' => TRUE,
        '#empty' => $this->t('No notifications found.'),
      ];
    $form['pager'] = array(
      '#type' => 'pager'
    );
      $form['#attached']['library'][] = 'core/drupal.tableselect';
    $form['#attached']['library'][] = 'metrobank_maillog/maillog.filter';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $from_date = $values["from_date"] ?? '';
    $to_date = $values["to_date"] ?? '';
    $from = $values["from"] ?? '';
    $to = $values["to"] ?? '';
    $subject = $values["subject"] ?? '';


    $form_state->setRedirect('metrobank_maillog.list_maillog', [
      'from_date' => $from_date,
      'to_date' => $to_date,
      'from' => $from,
      'to' => $to,
      'subject' => $subject,
    ]);
  }

  /**
   * Clear all the maillog entries.
   */
  public function clearMailLog(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('metrobank_maillog.mail_clear_log');
  }

  /**
   * @param $form
   * @param $form_state
   * @return void
   */
  function resetForm(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('metrobank_maillog.list_maillog');
  }
}
